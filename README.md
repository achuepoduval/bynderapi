# Bynderapi

Test cases

Get get top rated movies for a language "US-EN" on page 5
Validated response code, instance of the response, number displayed on page in the response

Passing invalid auth key and validated the response code

Passed invalid language and page value, response were positive and response code was 200.

Rate a new movie

Rate an already rated movie

Rate a movie with a higher value than expected


Rate a movie with a lower value

Rate a movie with an invalid api

Rate a movie with an invalid movie ID


Implemented Pytest

to execute the test :

Go to the repo's folder.
cd test (Navigate to the test fodler)
pytest -v


